<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    protected $table = 'categories';
    protected $guarded = [];

    public function children(){
        return $this->hasMany('App\Good','parent_id');
    }

    public function parent(){
        return $this->belongsTo('App\Good','parent_id');
    }

    public function items(){
        return $this->hasMany('App\Sold','item_id');
    }
}
