<?php

namespace App\Http\Controllers;

use App\Good;

use App\Sold;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\DataTables;


class GoodsController extends Controller
{
    public function itemIndex(){
        $items = Good::whereNull('parent_id')->pluck('id','name');
        if(\request()->ajax()) {
            $lists = DB::select(DB::raw("SELECT b.image AS 'image',a.name AS 'category',b.name AS 'item', b.parent_id AS 'parent_id' FROM categories a,categories b WHERE a.id = b.parent_id"));
            return DataTables::of($lists)->make();
        }
        return view('projectwork.goods.show-items',compact('items'));
    }

    public function storeItem(Request $request){
        $this->validate($request,[
            'name' => 'required'
        ]);
        $data = $request->all();
//        if($request->hasFile('image')){
//           $file = $request->file('image');
//           $fileName = time().'-'.rand(1111,9999).'.'.$file->getClientOriginalExtension();
//           $fileDestination = public_path('uploads/goods/');
//           $file->move($fileDestination,$fileName);
//            $data['image']=$fileName;
//        }
        $item = Good::create($data);
        $message = ['success' => 'data inserted successfully'];
        return Response::json($message);
    }

    public function sellItem(){
        $categories = Good::whereNull('parent_id')->pluck('id','name')->all();
        $users = User::all()->except(Auth::id());
        return view('projectwork.goods.sell-item',compact('categories','users'));
    }

    public function selectItem(Request $request)
    {
        $categories = Good::whereNull('parent_id')->pluck('id', 'name');
        $users = User::all()->except(Auth::id());
        $category = $request->category;
        if (\request()->ajax()) {
            $item = DB::table('categories')->where('parent_id', '=', $category)->pluck('name', 'id')->all();
            return Response::json(['options' => $item]);
        }
        return view('projectwork.goods.sell-item',compact('categories','users'));
    }

    public function solditemindex(){
        if(\request()->ajax()) {
            $sitem = Sold::with('category','users')->get();
            return DataTables::of($sitem)->make();
        }
        return view('projectwork.goods.sold-item',compact('sitem'));
    }
    public function itemSold(Request $request){
        $solditem = new Sold();
        $solditem->item_id = $request->items;
        $solditem->amount = $request->amount;
        $solditem->user_id = $request->user_id;
        $solditem->save();

    }
}
