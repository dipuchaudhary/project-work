<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\DataTables;


class ReportController extends Controller
{

    public function finalReport()
    {

            $userdonation = DB::table('users')
                ->join('donations', 'donations.user_id', '=', 'users.id')
                ->select('users.name', 'donations.description as desc','donations.amount as income','donations.created_at as date')
                ->get();
            $userexpenditure = DB::table('users')
                ->join('expenditures', 'expenditures.user_id', '=', 'users.id')
                ->select('expenditures.name as name','expenditures.description as desc','expenditures.amount as expense','expenditures.created_at as date')
                ->get();
            $usersolditem = DB::table('users')
                ->join('sold_items', 'sold_items.user_id', '=', 'users.id')
                ->join('categories','categories.id','=','sold_items.item_id')
                ->select('sold_items.amount as income', 'users.name as soldto','categories.name as name','sold_items.created_at as date')
                ->get();

       $reports = collect($userdonation)->merge($userexpenditure)->merge($usersolditem);

//        dd($reports);
//        return \response()->json($reports);

        //compact('reports')
        return view('projectwork.report.final-report',compact('reports'));
    }
}
