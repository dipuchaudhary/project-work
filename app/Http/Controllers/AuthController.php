<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Session;
use Yajra\DataTables\DataTables;

class AuthController extends Controller
{
    public function login(){
        return view('projectwork.auth.login');
    }

    public function postLogin(Request $request){
        $this->validate($request,[
           'email'=> 'required',
           'password'=> 'required'
        ]);
        $credentials = $request->only('email','password');
        if(Auth::attempt($credentials)){
            if(auth()->user()->role == 'admin'){
                return redirect()->intended(route('dashboard'));
            }else if(auth()->user()->role =='user') {
                return redirect()->intended(route('dashboard'));
            }
            else{
                return redirect()->back();
            }

        }
    }

    public function register(){
        return view('projectwork.auth.register');
    }

    public function postRegister(Request $request){
        $this->validate($request,[
           'name' => 'required',
           'email' => 'required|unique:users',
           'password' => 'required|min:6',
           'c_password' => 'required|same:password',
            'role' => 'required'
        ]);
        if(\request()->ajax()) {
            $data = $request->all();
            $check = $this->create($data);
            return Response::json($check);
        }

    }

    public function create(array $data){
        return User::create([
            'name'=>$data['name'],
            'email'=>$data['email'],
            'role' =>$data['role'],
            'password'=>Hash::make($data['password'])
        ]);
    }

    public function logout(){
        Session::flush();
        Auth::logout();
        return redirect('/signin');

    }

    public function userList(){
        if (\request()->ajax()) {
            $user = User::latest()->get();
            return DataTables::of($user)
                            ->addColumn('action',function ($user){
                                $button = '<button type="button" name="edit" id="'.$user->id.'" class="edit btn btn-primary">Edit</button>';
                                $button.= '&nbsp;&nbsp;<button type="button" name="delete" id="'.$user->id.'" class="delete btn btn-danger">Delete</button>';
                                return $button;
                            })->rawColumns(['action'])->make(true);
        }
        return view('projectwork.user.add-user');
    }

    public function edit($id){
        if(\request()->ajax()) {
            $data = User::findorfail($id);
            $pwd = Auth::user()->getAuthPassword();
            return Response::json(['result'=>$data,'pwd'=>$pwd]);
        }
    }

    public function update(Request $request,$id){

        if (\request()->ajax()){
            $this->validate($request,[
                'name' => 'required',
                'email' => 'required|email'
            ]);
            if (!is_null($request->password)){
                $this->validate($request,[
                    'password' => 'required|min:6',

                ]);
            }
            $user= User::findorfail($id);
            $pwd = Hash::make($request->password);
            $user->update($request->except('password')+['password'=>$pwd]);
            return Response::json(['success','user profile updated']);

        }
    }

    public function delete($id){
        $user = User::findorfail($id);
        $user->delete();
        return response()->json(['success', 'user deleted successfully']);

    }
}
