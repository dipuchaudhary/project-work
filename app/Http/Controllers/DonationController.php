<?php

namespace App\Http\Controllers;

use App\Donation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;


class DonationController extends Controller
{
    public function donationForm(){
        return view('projectwork.donation.add');
    }

    public function receiveDonation(Request $request){
        $this->validate($request,[
            'description' => 'required',
            'amount' => 'required|numeric',
            'contact' => 'required|min:6|max:10',
        ]);
        $data = $request->all();
        $user_id = Auth::user()->id;
        $data['user_id'] = $user_id;
        Donation::create($data);
        toastr()->success('Thank you for helping others!');
        $notification = array([
           'message'=> 'Thank you for helping others!',
           'alert-type'=>'success'
        ]);
        return view('projectwork.donation.add')->with($notification);

    }

    public function donationCollected(){

        $donor_names = Donation::with('users')->select('user_id')->distinct()->get();

       return view('projectwork.donation.collection',compact('donor_names'));
    }


    public function donationList(Request $request){
        $donor_names = Donation::with('users')->select('user_id')->distinct()->get();

        if(\request()->ajax()) {
            $id = $request->user_id;
            $from = $request->from_date;
            $to = $request->to_date;
            if ($id != '' && $from != '' && $to != '') {
                $data = Donation::with('users')->where('user_id', '=', $id)->whereBetween('created_at', [$from, $to])->get();;

            }
            else{
                $data = Donation::with('users')->get();
            }
                return DataTables::of($data)->make(true);
        }

        return view('projectwork.donation.collection',compact('donor_names'));
    }
}
