<?php

namespace App\Http\Controllers;

use App\Donation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $totalusers = User::all()->count();
        $donationamt = DB::table('donations')->get()->sum('amount');
        $salesamt = DB::table('sold_items')->get()->sum('amount');
        $expenditure = DB::table('expenditures')->get()->sum('amount');
        return view('projectwork.index',compact('totalusers','donationamt','salesamt','expenditure'));
    }
}
