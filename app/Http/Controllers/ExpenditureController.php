<?php

namespace App\Http\Controllers;

use App\Expenditure;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\DataTables;

class ExpenditureController extends Controller
{
    public function showExpenditureForm(){
        return view('projectwork.expenditure.add-expense');
    }

    public function storeExpenditure(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'amount' => 'required'
        ]);
        if(\request()->ajax()) {
            $expense_data = Expenditure::create($request->all());
            return Response::json($expense_data);
        }
//        return view('projectwork.expenditure.expense-list');

    }

    public function expenditureList(){
        if (\request()->ajax()){
            $expense_list = Expenditure::all();
            return DataTables::of($expense_list)->make();
        }
        return view('projectwork.expenditure.expense-list',compact('expense_list'));
    }
}
