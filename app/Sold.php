<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sold extends Model
{
    protected $table = 'sold_items';
    protected $guarded = [];

    public function category(){
        return $this->belongsTo('App\Good','item_id');
    }
    public function users(){
        return $this->belongsTo('App\User','user_id');
    }
}
