<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Donation extends Model
{
    protected $table = 'donations';
    protected $guarded = [''];


    public function users(){
        return $this->belongsTo('App\User','user_id');
    }
}
