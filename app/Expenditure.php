<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenditure extends Model
{
    protected $table = 'expenditures';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
