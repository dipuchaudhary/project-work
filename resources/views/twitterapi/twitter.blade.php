@extends('layouts.app')
@section('content')

        <div class="container mt-2">
            <div class="col-lg-6">
                <div class="bs-component">
                    <form action="{{route('post.tweet')}}" enctype="multipart/form-data" method="post">
                        @csrf
                        <fieldset>
                            <legend class="font-weight-bold">Tweets Here</legend>
                            <div class="form-group mt-3">
                                <h5>tweet text</h5>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="tweet" placeholder="Enter tweet description">
                                    @error('tweet')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Upload Image</label>
                                <input type="file" name="images[]" multiple class="form-control-file">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-info" value="Tweet">Tweet Here</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    <br>

    <div class="container">
        <h2 class="text-md-center badge-primary">Get user post using twitter API</h2>
        <div class="row">
            <div class="col-md-9">
                <div class="card mb-3 mt-4">
                    <img style="height: 300px; width: 100%; display: block;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22318%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20318%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_158bd1d28ef%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A16pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_158bd1d28ef%22%3E%3Crect%20width%3D%22318%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22129.359375%22%20y%3D%2297.35%22%3EImage%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image">
                    <div class="card-body">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
