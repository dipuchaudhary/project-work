@extends('projectwork.layouts.main')
@section('content')
    @include('projectwork.common.breadcrum')
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <a href="{{ url('pdf') }}" class="btn btn-success mb-2">Export PDF</a>
                    <table class="table table-bordered" id="laravel_crud">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Description</th>
                            <th>Name</th>
                            <th>Income</th>
                            <th>Expense</th>
                            <th>Balance</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($reports as $report)
                            <tr>
                                <td>{{ date('d-m-Y', strtotime($report->date)) }}</td>
                                <td>{{$report->desc}}</td>
                                <td>{{ $report->name }}</td>
                                <td>{{ $report->income }}</td>
                                <td>{{ $report->expense }}</td>
                                <td>{{$report->soldto}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection
