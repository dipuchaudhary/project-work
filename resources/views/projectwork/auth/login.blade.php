<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/custom-style.css')}}">
    <title>Login page</title>
</head>
<body>
<div class="container-fluid register">
    <div class="row">
        <div class="col-md-5 register-left">
            <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
            <h3>Welcome</h3>
            <p>to Login page</p>
        </div>
        <div class="col-md-7 register-right">
                    <div class="row register-form">
                        <div class="col-md-12">
                            <form action="{{route('signin')}}" method="post">
                                @csrf
                                <h3 class="text-center">Login Here</h3>
                            <div class="form-group">
                                <h6>Email:</h6>
                               <input type="email" class="form-control" name="email" placeholder="Enter your Email" value="{{old('email')}}" />
                                @error('email')
                                <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <h6>Password:</h6>
                                <input type="password" class="form-control" name="password" placeholder="Password" value="{{old('password')}}" />
                                @error('password')
                                <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                        <input type="submit" class="btn btn-primary btn-lg" value="Login"/>
                       </form>
                    </div>
                </div>
            </div>
    </div>
</div>
</body>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</html>
