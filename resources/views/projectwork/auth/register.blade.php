<div class="modal" tabindex="-1" role="dialog" id="user-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="user-form">
                    @csrf
                    <div class="form-group">
                        <label class="form-control-label">Full Name</label>
                        <input type="text" name="name" id="name" placeholder="Enter full Name" class="form-control" value="{{old('name')}}">
                        @error('name')
                        <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="nf-email" class=" form-control-label">Email</label>
                        <input type="email" id="email" name="email" placeholder="Enter Email.." class="form-control" value="{{old('email')}}">
                        @error('email')
                        <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="nf-password" id="pwd" class=" form-control-label">Password</label>
                        <input type="password" id="password" name="password" placeholder="Enter Password.." class="form-control" value="{{old('password')}}">
                        @error('password')
                        <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group cp">
                        <label for="nf-password" id="cpwd" class=" form-control-label">Confirm Password</label>
                        <input type="password" id="c_password" name="c_password" placeholder="Confirm Password.." class="form-control" value="{{old('c_password')}}">
                        @error('c_password')
                        <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Select Role</label>
                        <select class="form-control" name="role" id="role">
                            <option value="user" selected>user</option>
                            <option value="admin">admin</option>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="update-btn" class="btn btn-primary" id="add-btn">Add User</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
