@extends('projectwork.layouts.main')
@section('content')
    @include('projectwork.common.breadcrum')
    @include('projectwork.expenditure.add-expense')
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
                <button class="btn btn-primary ml-4" id="expense-btn">Add Expenditure</button>
                @endif
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Expenditure List</strong>
                        </div>
                        <div class="card-body">
                            <table id="expense-data-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Expense title</th>
                                    <th>Description</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
@section('custom-js')
    <script>
        $(document).ready(function () {
            <!--expenditure modal-->
            $('#expense-data-table').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'copy',
                    {
                        extend: 'csv',
                        exportOptions: {
                            stripHtml: false,

                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            stripHtml: false,

                        }
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            stripHtml: false,

                        }
                    },
                    {
                        extend: 'print',
                        exportOptions: {
                            stripHtml: false,

                        }
                    },
                ],
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{!! route('expenditure.list') !!}",
                    "type": "get",
                },
                columns: [
                    {data:"name"},
                    {data:"description"},
                    {data:"amount"},
                ],
            });
            $('#expense-btn').click(function () {
                $('#expense-modal').modal('show');
                $('#expense-form').trigger("reset");

                <!--add expense event-->
                $('#add-expense').click(function (e) {
                        e.preventDefault();
                        $.ajax({
                            url: "{!! route('expenditure.store') !!} ",
                            type: "post",
                            data: $('#expense-form').serialize(),
                            dataType: "json",
                            success: function(data){
                                $('#expense-form').trigger("reset");
                                $('#expense-modal').modal('hide');
                                $('#expense-data-table').DataTable().ajax.reload();
                                toastr.success('Expenses Added!', {progressBar: true,timeOut:5000});
                            },
                            error: function (data) {
                                console.log(data);
                                $('#expense-modal').modal('show');

                            }
                        })
                });
            });
        });
    </script>
@endsection
