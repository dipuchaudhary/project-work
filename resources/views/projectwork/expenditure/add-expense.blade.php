<div class="modal" tabindex="-1" role="dialog" id="expense-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <div class="modal-header">
                <h5 class="modal-title">Add Expenditure</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <form action="" method="post" id="expense-form">
                        @csrf
                        <input type="hidden" name="user_id" id="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">
                                <div class="form-group">
                                    <label for="description" class=" form-control-label">Expense Title</label>
                                    <input type="text" name="name" id="name" placeholder="Expense title" class="form-control" value="{{old('name')}}">
                                    @error('name')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="amount" class=" form-control-label">Amount</label>
                                    <input type="number" name="amount" id="amount" placeholder="Expense amount" class="form-control" value="{{old('amount')}}">
                                    @error('amount')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="description" class=" form-control-label">Description <span class="text-muted">(optional)</span></label>
                                    <input type="text" name="description" id="description" placeholder="description" class="form-control" value="{{old('description')}}">
                                    @error('description')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="add-expense">Add Expenses</button>
                </div>
               </form>
            </div>
        </div>
    </div>
</div>
