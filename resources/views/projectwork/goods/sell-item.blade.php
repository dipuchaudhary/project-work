@extends('projectwork.layouts.main')
@section('content')
    @include('projectwork.common.breadcrum')
    <div class="content">
        <div class="animated fadeIn">
                <form action="{{route('item.sold')}}" method="post" class="form-row" id="sell-item-form">
                   @csrf
                <div class="form-group col-md-3">
                    <label for="select" class=" form-control-label">Select Category</label>
                        <select name="category" id="category" class="form-control">
                            <option>--- Select Category ---</option>
                            @foreach($categories as $key=>$value)
                            <option value="{{$value}}">{{$key}}</option>
                                @endforeach
                        </select>
                   </div>
                    <div class="form-group col-md-3">
                        <label for="select" class=" form-control-label">Select Items</label>
                            <select name="items" id="items" class="form-control">
                                <option>--- Select Items ---</option>
                                @if(!empty($item))
                                    @foreach($item as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                @endif
                            </select>
                       </div>
                        <div class="form-group col-md-2">
                        <label for="select" class=" form-control-label">Amount</label>
                                <input type="number" name="amount" id="amount" class="form-control" placeholder="Enter selling price">
                        </div>
                    <div class="form-group col-md-2">
                        <label for="select" class=" form-control-label">To</label>
                        <select name="user_id" id="user_id" class="form-control">
                            <option>--- Select User ---</option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mt-md-4 col-md-2">
                        <label for="select" class=" form-control-label"></label>
                            <input type="submit" class="btn btn-primary" value="Submit" id="item-submit">
                    </div>
           </form>
        </div>
    </div>

@endsection

@section('custom-js')
<script>
    $('document').ready(function () {

        if($("#category").find('option.selected').val()===''){
            $('#items').attr('disabled',true);
        }

        $("select[name='category']").change(function (e) {
            e.preventDefault();
            var category = $(this).val();

            $.ajax({
                url: "{!! route('item.select') !!}",
                type: "post",
                dataType: "json",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "category": category,
                },
                success: function (data) {
                    $("select[name='items']").html('');
                    $.each(data.options, function (key, value) {

                        $("select[name='items']").append('<option value=' + key + '>' + value + '</option>');

                    });
                }

            })
        });

    });
</script>
@endsection
