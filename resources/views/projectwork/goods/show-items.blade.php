@extends('projectwork.layouts.main')
@section('content')
    @include('projectwork.common.breadcrum')
    @include('projectwork.goods.add-item')
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                @if(auth()->user()->role == 'admin')
                <button class="btn btn-primary ml-4" id="additem">Add Item</button>
            @endif
                <div class="col-md-12 ">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Item List</strong>
                        </div>
                        <div class="card-body">
                            <table id="item-data-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Items</th>
{{--                                    <th>Image</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

@section('custom-js')
    <!-- add item modal-->
    <script>
        $(document).ready(function () {
            <!--datatables js-->
            $('#item-data-table').dataTable({
                dom: 'lBfrtip',
                buttons: [
                    'copy',
                    {
                        extend: 'csv',
                        exportOptions: {
                            stripHtml: false,

                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            stripHtml: false,

                        }
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            stripHtml: false,

                        }
                    },
                    {
                        extend: 'print',
                        exportOptions: {
                            stripHtml: false,

                        }
                    },
                ],
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{!! route('item.index') !!}",
                    "type": "get",
                    "dataSrc": "data"
                },
                columns: [
                    {data:"category"},
                    {data:"item"},
                    // {data:"image",
                    //     targets:2,
                    //     "render": function (data) {
                    //         return '<img width="100px" src="/uploads/goods/' +data+ '" />';
                    //     }
                    // },
                ],
            });
<!--add model-->
            $('#additem').click(function () {
                $('#item-modal').modal('show');
                $('#item-modal').trigger("reset");
            });

            $('#save-item').click(function (e) {
                e.preventDefault();
                $.ajax({
                    url: '{!! route('item.store') !!}',
                    type: 'post',
                    data: $('#item-form').serialize(),
                    success: function (data) {
                            console.log(data);
                            $('#item-modal').modal('hide');
                            $('#item-form').trigger("reset");
                            toastr.success('success','item stored!',{progressBar:true,timeOut:5000});
                    },
                    error: function (data) {
                        console.log(data);
                        $('#item-modal').modal('show');
                    }
                })
                $('#item-data-table').DataTable().ajax.reload();
            });

        });

    </script>


@endsection
