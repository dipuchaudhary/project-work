@extends('projectwork.layouts.main')
@section('content')
    @include('projectwork.common.breadcrum')
    <div class="content">
        <div class="animated fadeIn">
            <div class="col-md-12 ">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Sold Items</strong>
                    </div>
                    <div class="card-body">
                        <table id="sold-data-table" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Item Name</th>
                                <th>Sold To</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function () {
                <!--datatables js-->
                $('#sold-data-table').dataTable({
                    dom: 'lBfrtip',
                    buttons: [
                        'copy',
                        {
                            extend: 'csv',
                            exportOptions: {
                                stripHtml: false,

                            }
                        },
                        {
                            extend: 'excel',
                            exportOptions: {
                                stripHtml: false,

                            }
                        },
                        {
                            extend: 'pdf',
                            exportOptions: {
                                stripHtml: false,

                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                stripHtml: false,

                            }
                        },
                    ],
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: '/goods/sold',
                        type:'get',
                    },
                    columns: [
                        {data: "created_at"},
                        {data: 'category.name'},
                        {data: "users.name"},
                        {data: "amount"},

                    ],

                });
        });
    </script>
@endsection
