<div class="modal" tabindex="-1" role="dialog" id="item-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times</span>
                </button>
            </div>
{{--            @if (count($errors)>0)--}}
{{--                @foreach ($errors->all() as $error)--}}
{{--                    <div class="alert alert-danger">--}}
{{--                        {{$error}}--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--            @endif--}}
{{--            @if(session('error'))--}}
{{--                <div class="alert alert-danger">--}}
{{--                    {{$error}}--}}
{{--                </div>--}}
{{--            @endif--}}

            <form action="{{route('item.store')}}"  method="post" id="item-form" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                        <div class="form-group">
                            <label for="description" class=" form-control-label">Items Name</label>
                            <input type="text" name="name" id="name" placeholder="Item name" class="form-control" value="{{old('name')}}">
                            @error('name')
                            <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="control-label">Choose Parent Item</label>
                            <select class="form-control form-white" data-placeholder="Choose a color..." name="parent_id" id="parent_id">
                                <option value="" selected>Default</option>
                                @foreach($items as $key=>$value)
                                    <option value="{{$value}}">{{$key}}</option>
                                @endforeach
                            </select>
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label for="image" class="control-label">Upload Image</label>--}}
{{--                            <input type="file" name="image" id="image" class="form-control-file"/>--}}
{{--                        </div>--}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="save-item">Save</button>
                    </div>
                </form>
        </div>
    </div>
</div>
