@extends('projectwork.layouts.main')
@section('content')
    @include('projectwork.common.breadcrum')
    @include('projectwork.auth.register')
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                @if(auth()->user()->role == 'admin')
                    <button class="btn btn-primary ml-4" id="adduser">Add User</button>
                @endif
                <div class="col-md-12 ">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">User List</strong>
                        </div>
                        <div class="card-body">
                            <table id="user-data-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function () {
            <!--datatables js-->
            $('#user-data-table').dataTable({
                dom: 'lBfrtip',
                buttons: [
                    'copy',
                    {
                        extend: 'csv',
                        exportOptions: {
                            stripHtml: false,

                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            stripHtml: false,

                        }
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            stripHtml: false,

                        }
                    },
                    {
                        extend: 'print',
                        exportOptions: {
                            stripHtml: false,

                        }
                    },
                ],
                processing: true,
                serverSide: true,
                ajax: {
                    "type": "get",
                    "url": "{!! route('user.register') !!}",
                    "dataSrc": "data"
                },
                columns: [
                    {data:"name"},
                    {data:"email"},
                    {data:"role"},
                    {data:"action",name:'action',orderable:false},
                ],
            });
            <!--add model-->
            $('#adduser').click(function () {
                $('#user-modal').modal('show');
                $('#user-form').trigger("reset");
                $('.modal-title').html("<h5>Add New User</h5>");
            });

            $('#add-btn').click(function (e) {
                e.preventDefault();
                $.ajax({
                    url: '{!! route('user.register') !!}',
                    type: 'post',
                    data: $('#user-form').serialize(),
                    success: function (data) {
                        console.log(data);
                        $('#user-modal').modal('hide');
                        $('#user-form').trigger("reset");
                        $('#user-data-table').DataTable().ajax.reload();
                        toastr.success('user added successfully!',{progressBar:true,timeOut:5000});
                    },
                    error: function (data) {
                        console.log(data);
                        $('#user-modal').modal('show');
                    }
                })
            });

            // Edit record
            $('#user-data-table').on('click', '.edit', function (e) {
                e.preventDefault();
                    $('#user-modal').modal('show');
                    $('.modal-title').html('Update user profile');
                    var id = $(this).attr('id');
                    console.log(id);
                    $.ajax({
                        url:"/signup/"+id+"/edit",
                        type: "get",
                        dataType: "json",
                        success: function (data) {
                            console.log(data.result);
                            console.log(data.pwd);
                            $('#name').val(data.result.name);
                            $('#email').val(data.result.email);
                            $("#role option:selected").attr('disabled','disabled')
                                .siblings().removeAttr('disabled').hide();
                            $('#pwd').text('New password');
                            $('#password').val(data.pwd);
                            $('#cpwd').remove();
                            $('#c_password').remove();
                            $('#add-btn').text('Update');
                        }
                    });
                //update record
                $('#add-btn').click(function () {
                    $.ajax({
                        url:'signup/update/'+id,
                        type:'post',
                        data: $('#user-form').serialize(),
                        success: function (data) {


                            $('#user-modal').modal('hide');
                            $('#user-form').trigger("reset");
                            $('#user-data-table').DataTable().ajax.reload();
                            toastr.success('user updated successfully!',{progressBar:true,timeOut:5000});
                        },
                        error: function (data) {
                            console.log(data);
                            $('#user-modal').modal('show');
                        }
                    })
                })

            } );


            // Delete a record
            $('#user-data-table').on('click', '.delete', function (e) {
                e.preventDefault();
                var id = $(this).attr('id');
                if(confirm('Are you sure want to delete?')){
                $.ajax({
                    url: "/signup/"+id+"/delete",
                    type:'delete',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": id,
                        },
                    success: function(data){
                          $('#user-data-table').DataTable().ajax.reload();
                          toastr.success('user deleted successfully!',{progressBar:true,timeOut:5000});
                    }
                })

                }
                return false;

            } );

        });

    </script>

@endsection
