@extends('projectwork.layouts.main')
@section('content')
    @include('projectwork.common.breadcrum')
    <div class="content">
        <div class="animated fadeIn">
            <form method="post" class="form-row" id="input-daterange">
                @csrf
                <div class="form-group col-md-3">
                    <label for="select" class=" form-control-label">Select Donor Name</label>
                    <select  class="form-control" name="user_id" id="user_id">
                        <option value="">Please select</option>
                        @foreach($donor_names as $donor)
                            <option value="{{$donor->user_id}}">{{$donor->users->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="select" class=" form-control-label">Date From:</label>
                    <input type="date" name="from_date" id="from_date" class="form-control">
                </div>
                <div class="form-group col-md-3">
                    <label for="select" class=" form-control-label">Date To:</label>
                    <input type="date" name="to_date" id="to_date" class="form-control">
                </div>
                <div class="form-group mt-md-4 col-md-3">
                    <input type="submit" class="btn btn-primary" value="Filter" id="filter">
                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Donation collected List</strong>
                        </div>
                        <div class="card-body">
                            <div id="button-group"></div>
                            <table id="data-table" class="table table-striped table-bordered display-nowrap table-data">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Donor Name</th>
                                    <th>Donation for</th>
                                    <th>Contact</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@section('custom-js')
    <!--dataTables ajax-->
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            load_data();
            function load_data(id='',from_date='',to_date='') {
                $('#data-table').dataTable({
                    dom: 'lBfrtip',
                    buttons: [
                        'copy',
                        {
                            extend: 'csv',
                            exportOptions: {
                                stripHtml: false,

                            }
                        },
                        {
                            extend: 'excel',
                            exportOptions: {
                                stripHtml: false,

                            }
                        },
                        {
                            extend: 'pdf',
                            exportOptions: {
                                stripHtml: false,

                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                stripHtml: false,

                            }
                        },
                    ],
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{!! route('donation.collected') !!}",
                        type: "post",
                        data: function (d) {
                            return $('#input-daterange').serialize();
                        },
                        datasrc: "",
                    },
                    columns: [
                        {data: 'created_at'},
                        {data: "users.name"},
                        {data: 'description'},
                        {data: 'contact'},
                        {data: 'amount'}
                    ],
                });
            }
            $('#filter').click(function (e) {
                e.preventDefault();
                var user_id = $('#user_id').val();
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();

                if(user_id !='' && from_date !='' && to_date !=''){
                    $('#data-table').DataTable().destroy();
                    console.log(user_id,from_date,to_date);
                    load_data(user_id,from_date,to_date);
                    $('#user_id').val('');
                    $('#from_date').val('');
                    $('#to_date').val('');
                }
                else if(user_id !='' && from_date ==''){
                    alert('Both date is required');
                }
                else{
                    $('#user_id').val();
                    $('#from_date').val('');
                    $('#to_date').val('');
                    $('#data-table').DataTable().destroy();
                    load_data();
                }
            });
        });
    </script>
@endsection
