@extends('projectwork.layouts.main')
@section('content')
@include('projectwork.common.breadcrum')
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
         <div class="col-md-12 ">
          <form action="{{route('donation.receive')}}" method="post">
              @csrf
          <div class="card">
            <div class="card-header"><strong>Donation</strong><small> Form</small></div>
            <div class="card-body card-block">
                <div class="form-group">
                    <label for="description" class=" form-control-label">Description</label>
                    <input type="text" name="description" id="description" placeholder="donation for" class="form-control" value="{{old('description')}}">
                    @error('description')
                    <small class="text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="amount" class=" form-control-label">Amount</label>
                    <input type="number" name="amount" id="amount" placeholder="Any amount starting from 1" class="form-control" value="{{old('amount')}}">
                    @error('amount')
                    <small class="text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="contact" class=" form-control-label">Contact</label>
                    <input type="number" name="contact" id="contact" placeholder="Enter contact number" class="form-control" min="6" maxlength="10" value="{{old('contact')}}">
                    @error('contact')
                    <small class="text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="purpose" class=" form-control-label">Purpose <span class="text-muted">(optional)</span></label>
                    <input type="text" name="purpose" id="purpose" placeholder="Purpose" class="form-control" value="{{old('purpose')}}">
                    @error('purpose')
                    <small class="text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-actions form-group">
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </div>
            </div>
           </form>
       </div>
            </div>
        </div>
    </div>

@endsection
