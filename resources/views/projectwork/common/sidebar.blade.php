<!-- Left Panel -->
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm
    navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{url('/dashboard')}}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                </li>
                <li class="menu-title"><i class="menu-icon fa fa-bitcoin"></i> Donation</li><!-- /.menu-title -->
                <li class="menu-item">
                    <a href="{{url('/donation/add')}}"><i class="menu-icon fa fa-money"></i> Donate</a>
                    @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
                    <a href="{{url('/donation/collection')}}" class="dropdown-toggle" aria-expanded="false"><i class="menu-icon fa fa-dollar"></i> Donation collected</a>@endif
                </li>
                <li class="menu-title"><i class="fa fa-rocket"></i> Expenditure</li><!-- /.menu-title -->

                <li class="menu-item">
                    <a href="{{url('/expenditure/list')}}"> <i class="menu-icon fa fa-bolt"></i>Show</a>
                </li>
                <li class="menu-title">Goods</li><!-- /.menu-title -->
                <li class="menu-item">
                    <a href="{{url('/goods/add')}}"> <i class="menu-icon fa fa-plus"></i>Add Items</a>
                    <a href="{{url('/goods/sell')}}"> <i class="menu-icon fa fa-th-list"></i>Sell Items</a>
                    <a href="{{url('/goods/sold')}}"> <i class="menu-icon fa fa-th-list"></i>Sold Items</a>
                </li>

                <li class="menu-title">Report</li><!-- /.menu-title -->
                <li class="menu-item">
                    <a href="/report/finalreport"> <i class="menu-icon fa fa-glass"></i>Show Report</a>
                </li>
                @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
                <li class="menu-title"><i class="menu-icon fa fa-users"></i> Add User</li><!-- /.menu-title -->
                <li class="menu-item">
                <a href="/signup"><i class="menu-icon fa fa-user"></i>Add User</a>
                </li>@endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>
<!-- /#left-panel -->
