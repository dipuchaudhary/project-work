Vue.component('product',{
    template:`
    <div class="product">
        <div class="product-image">
            <img v-bind:src="image">
        </div>
        <div class="product-info">
            <h1>{{title}}</h1>
            <p>{{sale}}</p>
            <p v-if="instock">Instock</p>
<!--            <p v-else-if="instock<=10 && instock>0">Almost sold out</p>-->
            <p v-else="instock":class="{error:!instock}">Outofstock</p>
            <ul>
                <li v-for="detail in details">{{detail}}</li>
            </ul>
            <p>Shipping:{{premium}}</p>
            <div  class="color-box" v-for="(variant,index) in variants":key="variant.variantId"
                :style="{backgroundColor:variant.variantColor}"
                  @mouseover="updateproduct(index)">
            </div>
            <button v-on:click="addtocart":disabled="!instock":class="{disabledButton:!instock}">Add to cart</button>

            <button v-on:click="removefromcart">Remove from cart</button>
           <div class="cart">
              <p>Cart ({{cart}})</p>
           </div>

        </div>
    </div>`,
    data() {
        return {
            product:'Socks',
            brand:'Vue mastery',
            description: 'This is the product description',
            variantSelected:0,
            onsale:true,
            user:true,
            details:['80% cotton','20% polyester','Gender-neutral'],
            variants:[
                {
                    variantId:222,
                    variantColor: 'green',
                    variantImage: 'img/socks.jpg',
                    variantQuantity:10,
                },
                {
                    variantId: 223,
                    variantColor: 'blue',
                    variantImage: 'img/blueshock.jpg',
                    variantQuantity: 0,
                }
            ],
            sizes:['small','medium','large'],
            cart:0
         }
    },
    methods:{
        addtocart() {
            this.cart+=1
        },
        updateproduct(index){
            this.variantSelected = index;
            console.log(index);
        },
        removefromcart(){
            this.cart-=1;
        }
    },
    computed:{
        title(){
            return this.brand +' '+this.product;
        },
        image(){
            return this.variants[this.variantSelected].variantImage;
        },
        instock(){
            return this.variants[this.variantSelected].variantQuantity;
        },
        sale(){
            if(this.onsale){
                return this.brand +' '+this.product +'are on sale!';
            }
            return this.brand+' '+ this.product+'are not on sale!';
        },
        premium(){
            if(this.user){
                return 'free';
            }
            return 2.99;
        }
    }

})

var app = new Vue({

    el: '#app',


});

