<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
Route::get('/', function () {
    return view('welcome');
});

//Route::get('/', function() {
//    return response()->json([
//        'stuff' => phpinfo()
//    ]);
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('users','HomeController@users')->name('users');
Route::get('user/{id}','HomeController@user')->name('user.view');
Route::post('follow','HomeController@followUserRequest')->name('follow');

Route::get('tweets','TwitterController@getUserTimeline');
Route::post('tweets',['as'=>'post.tweet','uses'=>'TwitterController@postTweet']);


Route::get('/vue',function (){
    return view('vuejs.index');
});

//dashboard

Route::get('signin','AuthController@login');
Route::post('signin','AuthController@postLogin')->name('signin');
Route::get('signup','AuthController@userList');
Route::post('signup','AuthController@postRegister')->name('user.register');
Route::get('/dashboard','DashboardController@index')->name('dashboard');
Route::get('/signout','AuthController@logout')->name('signout');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/signup/{id}/edit','AuthController@edit')->name('user.edit');
Route::post('/signup/update/{id}','AuthController@update')->name('user.update');
Route::delete('/signup/{id}/delete','AuthController@delete')->name('user.delete');
//donation
Route::get('/donation/add','DonationController@donationForm');
Route::post('/donation','DonationController@receiveDonation')->name('donation.receive');
Route::get('/donation/collection','DonationController@donationCollected');
Route::post('/donation/collection','DonationController@donationList')->name('donation.collected');


//expenditure
//Route::get('/expenditure/add','ExpenditureController@showExpenditureForm');
Route::post('/expenditure/list','ExpenditureController@storeExpenditure')->name('expenditure.store');
Route::get('/expenditure/list','ExpenditureController@expenditureList')->name('expenditure.list');

//goods
Route::get('/goods/add','GoodsController@itemIndex')->name('item.index');
Route::post('/goods/add','GoodsController@storeItem')->name('item.store');
Route::get('/goods/sell','GoodsController@sellItem');
Route::post('/goods/sell','GoodsController@selectItem')->name('item.select');
Route::get('/goods/sold','GoodsController@solditemindex');
Route::post('/goods/sold','GoodsController@itemSold')->name('item.sold');

//report
Route::get('/report/finalreport','ReportController@finalReport')->name('finalreport');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
